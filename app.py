from flask import Flask, render_template, request, redirect
import ast  # Dictionary lesen
import json # Dictionary schreiben

app = Flask(__name__)

# PREVENT THE BROWSER FROM CACHING
# this is good for developing. bad for deploying
@app.after_request
def set_response_headers(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response

def read():
    """
    Liest ein Dictionary aus der Datei "queries.config".
    Rückgabe: d (Dictionary) das augelesene Dictionary
    """
    # Datei im Flask-Root-Ordner öffnen
    with open(app.root_path + '/queries.config') as f:
        d = f.read()
        d = ast.literal_eval(d)
    return d

def write(data):
    """
    Schreibt ein Dictionary (data) in die Datei "queries2.config".
    """
    with open(app.root_path + '/queries2.config', 'w') as file:
        file.write(json.dumps(data))

data = read()
# Index page
@app.route('/', methods=["GET", "POST"])
def index():
    return render_template('index.html', data=data, qlen=len(data["queries"]))

# Definiere Seite mit URL "/edit/i", (i ist der Index des Dictionaries)
@app.route('/edit/<i>', methods=["GET", "POST"])
def edit(i):
    """
    Macht das Dictionary mit Index i editierbar
    """
    i = int(i)      # i zu Integer
    query = data["queries"][i]    # zu bearbeitendes Dictionary
    # "Speichern"-Button gedrückt?
    if request.method == "POST":
        # beschreibe Einträge
        for key,val in query.items():
            query[key] = request.form[key]
        # schreibe in die Datei
        write(data)
        # zurück zur Indexseite
        return redirect('/')
    return render_template('edit.html', query=query, i=i)

if __name__ == "__main__":
    app.run(processes=1,debug=True)
