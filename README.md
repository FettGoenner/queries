# queries

## Beschreibung
Dies ist eine kleine Web-Applikation zur Veranschaulichung der Datei
`queries.conf` aus diesem [DBMS-Benchmarker](https://github.com/Beuth-Erdelt/DBMS-Benchmarker).
Außerdem können Teile des Datei bearbeitet werden.

## Installation
Archiv hier herunterladen und entpacken, oder mit Git:
```console
$ git clone "https://gitlab.com/FettGoenner/queries"
```

## Nutzung

Erst muss im Terminal die Flask-App lokalisiert werden, dann kann gestartet werden:

Bash:
```console
$ export FLASK_APP="/pfad/zu/queries/app.py"
$ flask run
```
ZSH:
```console
$ export FLASK_APP="/pfad/zu/queries/app.py"
$ python3 -m flask run
```
CMD: 
```console
> set FLASK_APP=/pfad/zu/queries/app.py
> flask run
```

Powershell: 
```console
> env:FLASK_APP = "/pfad/zu/queries/app.py"
> flask run
```

Nun die URL [`http://127.0.0.1:5000/`](http://127.0.0.1:5000/) im Browser aufrufen.

## Support
Keiner.

## Contributing
Nicht offen für Zusammenarbeit.

## Autoren
P.M.

## Lizenz
Lizensiert unter der GNU GPLv3.

## Projektstatus
läuft!
